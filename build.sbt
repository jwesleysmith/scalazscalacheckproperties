val v_scala210        = "2.10.6"
val v_scala211        = "2.11.8"
val v_scalacheck      = "1.13.2"
val v_scalaz          = "7.2.6"

lazy val commonSettings = Seq(
  organization := "io.atlassian",
  scalaVersion := "2.11.8"
)


lazy val root = (project in file(".")).
  settings(commonSettings: _*).
  settings(
    name := "scalazcheck"
  , libraryDependencies ++= deps
  , scalaVersion := v_scala211
  , crossScalaVersions := Seq(v_scala211, v_scala210)
  , releaseCrossBuild := true
  , scalacOptions ++= scalacFlags
  , scalacOptions in (Compile, console) ~= (_ filterNot (_ == "-Ywarn-unused-import"))
  , scalacOptions in (Test, console) := (scalacOptions in (Compile, console)).value
  , javacOptions ++= Seq("-encoding", "UTF-8")
  , addCompilerPlugin("org.spire-math"    % "kind-projector"   % "0.8.1" cross CrossVersion.binary)
  )

lazy val deps =
  Seq(
    "org.scalaz"          %% "scalaz-core"       % v_scalaz
  , "org.scalaz"          %% "scalaz-effect"     % v_scalaz
  , "org.scalaz"          %% "scalaz-concurrent" % v_scalaz
  , "org.scalaz"          %% "scalaz-iteratee"   % v_scalaz
  , "org.scalacheck"      %% "scalacheck"        % v_scalacheck
  )

lazy val scalacFlags = Seq(
  "-deprecation"
  , "-encoding", "UTF-8" // yes, this is 2 args
  , "-feature"
  , "-language:_"
  , "-unchecked"
//  , "-Xfatal-warnings" // weird implicit numeric widening warning in 2.10
  , "-Xfuture"
  , "-Xlint"
  , "-Yno-adapted-args"
  , "-Ywarn-dead-code" // N.B. doesn't work well with the ??? hole
  , "-Ywarn-numeric-widen"
//  , "-Ywarn-value-discard" // used a lot in the original source
//  , "-Ywarn-unused" // not in scala 2.10
//  , "-Ywarn-unused-import" // not in scala 2.10
  , "-Xlog-free-terms"
  , "-Xmax-classfile-name", "134" // Some filesystems encryption schemes (e.g. ecryptfs) have file name length limits
  //  , "-Xprint:typer" // print implicit resolution
)
